#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SPR,音声班,リドルゲーム質疑応答,subscriber

import os # espeakで使用

import rospy
from std_msgs.msg import String, Bool

import difflib # 類似度を計算するライブラリ

qa_dict = {}

PATH = '/home/rione/catkin_ws/src/data/question.csv' # 問題と答えが書かれているcsvファイル

speak_flag = ''

#stringの文字を送る関数
def string_send(str,pub):
	send = String()
	send.data = str
	pub.publish(send)

# 類似度を計算する関数
def get_similar(listX, listY):
    s = difflib.SequenceMatcher(None, listX, listY).ratio()
    return s

# 受け取った質問文を処理する関数
def callback(data):
	global speak_flag # 発話が終了したかどうか
	question = data.data # 受け取った文をquestionとおく
	string_send('stop',pub_ju_stop)# 'stop'という文字列を送って、julius止める
	wait("speak_flag") # 'recognition'が返ってくるまで待つ

	#質問文と問題リストの類似度計算し一番類似度の高い答えを探す
	max = 0
	answer = ''
	question = question.decode('utf-8')

	for q,a in qa_dict.items():
		level = get_similar(question,q)
		if level > max:
			max = level
			answer = a

	print answer
	string_send(answer,pub_speak) # 発話の文字列を送る
	wait("speak_flag") # 'speaker'が返ってくるまで待つ
	string_send('start',pub_ju_start) # 'start'という文字列を送って、Juliusの認識再開

def listener():
	rospy.Subscriber('result', String, callback)
	rospy.init_node('listener', anonymous=True)
	rospy.spin()


# 発話終了のメッセージを受け取る関数
def speak_signal(message):
    global speak_flag
    speak_flag = message.data
    print speak_flag

# speak_flagが'recognition'か'speaker'になるまでループし続ける関数
def wait(target):
	if target == "speak_flag":
		global speak_flag		
		speak_flag = ''
    	while (speak_flag != 'recognition') and (speak_flag != 'speaker'):
    		pass   


if __name__ == '__main__':
	with open(PATH,'r') as f: # 問題と答えが書かれたファイルを開く(パスは適宜書き換える)
		qa_list = f.readlines()

	for qa in qa_list: # 辞書qa_dict{}に問題と答えを登録
		qa = qa.rstrip().decode('utf-8').split(';')
		qa_dict[qa[0]] = qa[1]

	pub_speak = rospy.Publisher('speaker', String, queue_size=10)
	pub_ju_start = rospy.Publisher('recognition_start', String, queue_size=10) # juliusの操作側に指示を送る(start)
	pub_ju_stop = rospy.Publisher('recognition_stop', String, queue_size=10) # juliusの操作側に指示を送る(stop)

	rospy.Subscriber('finish',String,speak_signal) # 発話が終わった指示を受けとる
    
	listener()
